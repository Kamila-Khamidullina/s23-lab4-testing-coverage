package com.hw.db.DAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.stream.Stream;

public class ThreadDAOTest {
    private JdbcTemplate mockJdbc;

    @BeforeEach
    void setUp() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ThreadDAO(mockJdbc);
    }

    private static Stream<Arguments> params() {
        return Stream.of(
                Arguments.of(0, 1, 1, true,
                        "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
                Arguments.of(0, 1, 1, false,
                        "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"));
    }

    @ParameterizedTest
    @MethodSource("params")
    void TestTreeSort(Integer id, Integer limit, Integer since, Boolean desc, String q) {
        ThreadDAO.treeSort(id, limit, since, desc);
        Mockito.verify(mockJdbc).query(Mockito.eq(q), Mockito.any(PostDAO.PostMapper.class), Mockito.any());
    }
}
