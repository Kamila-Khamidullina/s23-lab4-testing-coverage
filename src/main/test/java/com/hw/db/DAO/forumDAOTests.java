package com.hw.db.DAO;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.junit.jupiter.params.provider.Arguments;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.params.provider.MethodSource;

class ForumDAOTest {
    private JdbcTemplate mockJdbc;

    @BeforeEach
    void setUp() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
    }

    private static Stream<Arguments> params() {
        return Stream.of(
                Arguments.of("slug", 1, "since", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Arguments.of("slug", null, "since", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
                Arguments.of("slug", 1, "since", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Arguments.of("slug", null, "since", null, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                Arguments.of("slug", 1, null, true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Arguments.of("slug", null, null, true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc;"),
                Arguments.of("slug", 1, null, null, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"),
                Arguments.of("slug", null, null, null, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;")
        );
    }

    @ParameterizedTest
    @MethodSource("params")
    void TestUserList(String slug, Integer limit, String since, Boolean desc, String expected) {
        ForumDAO.UserList(slug, limit, since, desc);
        Mockito.verify(mockJdbc).query(Mockito.eq(expected), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }
}
